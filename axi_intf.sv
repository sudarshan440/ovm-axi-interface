interface axi_intf ();

parameter ADDR_WIDTH =32; 
parameter DATA_WIDTH= 32; 
parameter MEM_SIZE   =16; 
parameter AXI_MAS_ID =18; 
parameter AXI_WID =4;


logic clk;
logic  reset;
logic  wr;// for write wr=1;
          // for read  wr=0;

//Write adress channel to ddr
logic [ AXI_WID -1:0]             awid;
logic [ ADDR_WIDTH-1:0]           awaddr;
logic [3:0]                       awlen;
logic [2:0]                       awsize;
logic [1:0]                       awburst;
logic                             awvalid;
logic                             awready;

//Write data channel to ddr
logic [ AXI_WID -1:0]             wid;
logic [ DATA_WIDTH -1:0]          wdata;
logic [3:0]                       wstrb;
logic                             wlast;
logic                             wvalid;
logic                             wready;

//Write response channel from slave
logic 				              bready;
logic [ AXI_WID -1:0]             bid;
logic [1:0]                       bresp;
logic                             bvalid;

//Read address channel of slave
logic [ AXI_WID -1:0]             arid;
logic [31:0]                      araddr;
logic [3:0]                       arlen;
logic [2:0]                       arsize;
logic [1:0]                       arburst;
logic                             arvalid;
logic                             arready;

//Read data channel from slave
logic                             rready;
logic [ AXI_WID -1:0]             rid;
logic [1:0]                       rresp;
logic                             rlast;
logic                             rvalid;
logic [ DATA_WIDTH -1:0]          rdata;

endinterface
