class Wrapper extends ovm_object;
  virtual interface axi_intf intf;
  
  `ovm_object_utils(Wrapper)
  
  function new(string name = "Wrapper");
    super.new(name);
  endfunction : new
  
  function void setVintf(virtual interface axi_intf intf);
    this.intf = intf;
  endfunction : setVintf
  
endclass: Wrapper
