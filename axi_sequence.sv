class axi_sequence extends ovm_sequence#(axi_sequence_item);
axi_sequence_item seq_item;

//registering 
`ovm_object_utils(axi_sequence)

//constructor

function new(string name = " ");
super.new(name);
endfunction 

// body

virtual task body();

//write data to slave
repeat(500)
begin
$display("one trans strt");
seq_item = axi_sequence_item::type_id::create("seq_item");
wait_for_grant();
//void'(seq_item.randomize() with {seq_item.addr==8'b1001000;seq_item.wr==0;seq_item.data ==8'b10101010;});
void'(seq_item.randomize() with {seq_item.addr>0;seq_item.wr==0;seq_item.data > 0;});
send_request(seq_item);
wait_for_item_done();
$display("one trans done");
end

endtask : body
endclass : axi_sequence