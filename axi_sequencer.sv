class axi_sequencer extends ovm_sequencer#(axi_sequence_item);

//registering
`ovm_component_utils(axi_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : axi_sequencer