module top;
  import axiPkg::*;
  import axiTestPkg::*;

  
  axi_intf intf();

     
  axi_slave dut( 
				.awid(intf.awid),
				.aclk(intf.clk), 
				.awaddr(intf.awaddr),
				.awlen(intf.awlen),
				.awsize(intf.awsize),
				.awburst(intf.awburst),
				.awvalid(intf.awvalid),
				.awready(intf.awready),
				 .aresetn(intf.reset),  
				.wid(intf.wid),
				.wdata(intf.wdata),
				.wstrb(intf.wstrb),
				.wlast(intf.wlast),
				.wvalid(intf.wvalid),
				.wready(intf.wready),
				.bready(intf.bready),
				.bid(intf.bid),
				.bresp(intf.bresp),
				.bvalid(intf.bvalid),
				.arid(intf.arid),
				.araddr(intf.araddr),
				.arlen(intf.arlen),
				.arsize(intf.arsize),
				.arburst(intf.arburst),
				.arvalid(intf.arvalid),
				.arready(intf.arready),
				.rready(intf.rready),
				.rid(intf.rid),
				.rresp(intf.rresp),
				.rlast(intf.rlast),
				.rvalid(intf.rvalid),
				.rdata(intf.rdata)
				);
  
  
 
  

   initial begin    
      intf.clk = 0;
	  forever begin
        #5ns intf.clk = ~intf.clk;
      end
	end
	

	
initial
begin
Wrapper wrapper = new("Wrapper");
wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("axi_testcase");

end
endmodule
  