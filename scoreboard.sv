class scoreboard extends ovm_component;
`ovm_component_utils(scoreboard)
axi_sequence_item datafromwrite;
axi_sequence_item datafromread;
ovm_analysis_export #(axi_sequence_item) writesb;
ovm_analysis_export #(axi_sequence_item) readsb;
tlm_analysis_fifo #(axi_sequence_item) getwrite;
tlm_analysis_fifo #(axi_sequence_item) getread;

function new (string name = "", ovm_component parent = null);
super.new(name,parent);
datafromwrite = new();
datafromread = new();
endfunction : new

function void build();
super.build();
 writesb= new("writesb",this);
 readsb= new("readsb",this);
 getwrite= new("getwrite",this);
 getread= new("getread",this);

endfunction : build

function void connect();
writesb.connect(getwrite.analysis_export); //dut
readsb.connect(getread.analysis_export); //dut
endfunction : connect
  
task run();
  forever
begin
getwrite.get(datafromwrite);
$display("in scoreboard wite addr is %d data is %d",datafromwrite.addr,datafromwrite.data);

getread.get(datafromread);
$display("in scoreboard read addr is %d data is %d",datafromread.addr,datafromread.data);

$display("in scoreboard");
 compare(); 
end
endtask: run

function void compare();
 if(datafromwrite.addr == datafromread.addr && datafromwrite.data == datafromread.data)
  ovm_report_info(get_type_name(),"packet matches",OVM_LOG);
 else
  ovm_report_info(get_type_name(),"packet mismatches",OVM_LOG);
 
endfunction : compare

endclass : scoreboard