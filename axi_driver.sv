class axi_driver extends ovm_driver#(axi_sequence_item);
axi_sequence_item seq_item;
virtual axi_intf intf;
Wrapper wrapper;
ovm_object dummy;


//registering
`ovm_component_utils(axi_driver)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new


virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
	ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
	ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
  begin
	ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
	intf = wrapper.intf;
  end
else
	ovm_report_info(get_type_name(),"dummy is really dummy");

endfunction : build


//run phase
virtual task run ();

reset_dut();
		forever 
		begin
			seq_item_port.get_next_item(seq_item);
			$display("i pulled the transaction");
		    $display($time );
	    	slave_write(seq_item);
			$display($time );
			$display("driver write address is %d write data is %d",seq_item.addr,seq_item.data);
            $display($time );
            slave_read(seq_item); 
            $display($time );  
			$display("driver read address is %d read data is %d",seq_item.addr,seq_item.data);
            $display($time );
			seq_item_port.item_done();
		end
  
endtask : run


task slave_write(axi_sequence_item seq_item);
     @(posedge intf.clk);
    addrtransfer(seq_item.id,1'b1,seq_item.addr,4'b0000,3'b010,2'b01); 
    datatransfer(seq_item.id,1'b1,seq_item.data,8'hf0,1'b1);
     @(posedge intf.clk);
    slaveresponse(seq_item.id);
   // $display("@%0t [axi_driver] axi_write done", $time);  
endtask : slave_write
  
task addrtransfer(  input bit id ,input bit AWVALID, input bit [31:0] addr ,input bit [3:0] AWLEN,  input bit [2:0] AWSIZE,input bit [1:0] AWBURST);
 $display("@%0t [axi_driver]  write_addr_to_ddr started \n",$time);
    intf.awid       =  id;
    intf.awvalid  = AWVALID;
    intf.awaddr   =  addr;//32'hc0320010;//class_inq_pktptr
    intf.awlen    = AWLEN; //no of data transfers is 1
    intf.awsize   = AWSIZE;  //transfers 4 Bytes of data
    intf.awburst  = AWBURST;   //Incrementing address burst
	$display("awaddr at driver is %d ",addr); 
   @(posedge intf.clk);
 //$display("@%0t [axi_driver] waiting for slave awready==1  \n",$time);
    wait(intf.awready === 1'b1);
	//@(posedge intf.clk);
	$display("@%0t [axi_driver]  slave has taken waddr    \n",$time);
    intf.awid     = 0;
    intf.awvalid  = 0;
    intf.awaddr   = 0;
    intf.awlen    = 0; 
    intf.awsize   = 0; 
    intf.awburst  = 0;
  @(posedge intf.clk);
 //$display("@%0t [axi_driver]  write_addr_to_slave ended\n",$time);
endtask

task datatransfer(input bit id,input bit WVALID, input bit [31:0] data,input [7:0] WSTRB,  input bit WLAST);

 //$display("@%0t [axi_driver]  write_data_to_slave started \n",$time);
    intf.wid       =   id;
    intf.wvalid   =  WVALID;
    intf.wdata    =   data;
    intf.wstrb    =  WSTRB;
    intf.wlast    =  WLAST;
	$display("wdata at driver is %d",data);
     @(posedge intf.clk);
 
    wait (intf.wready === 1'b1);
	$display("@%0t [axi_driver] %m  wdata has taken by slave  \n",$time);
    intf.wid     =0; 
    intf.wvalid  =0;
    intf.wdata   =0;
    intf.wstrb   =0;
    intf.wlast   =0;
//$display("@%0t [axi_driver]  write_data_to_slave ended\n",$time);
endtask

task slaveresponse(input [17:0] BID);
 //$display("@%0t [axi_driver]  Waiting for responce from ddr started \n",$time);
//Write responce back to axi from ocp 
    intf.bready  =1'b1;
    wait (intf.bvalid === 1'b1);// && vif.cb.ddr2axi_bid == BID );
$display("@%0t [axi_driver]  Got the response from slave after waddr and wdata \n",$time); 
 @(posedge intf.clk);
 
endtask

task reset_dut();
    intf.reset = 0;
    #50ns ;
    intf.reset = 1;
       
endtask
   
task slave_read(axi_sequence_item seq_item);
  //$display("@%0t [axi_driver] axi_read started addr=%0h ", $time,seq_item.addr);  
  read_addr(seq_item.id,seq_item.addr);
  datareceive(seq_item.id,seq_item.data);
  //$display("@%0t [axi_driver] axi_read done read data =%0h",$time,seq_item.data);  
endtask

task read_addr(input bit [17:0] ARID,input bit [31:0] ARADDR);
 //$display("@%0t [axi_driver]  read_addr_slave started \n",$time);
    intf.arid     = ARID;
    intf.arvalid  = 1'b1;
    intf.araddr   = ARADDR;
    intf.arlen    = 0; //no of data transfers is 1
    intf.arsize   = 2;  //transfers 4 Bytes of data
    intf.arburst  = 1;   //Incrementing address burst
    $display("araddr at driver is %d   ", ARADDR); 
    @(posedge intf.clk);

    wait(intf.arready === 1'b1);
	$display("@%0t [axi_driver] slave has taken raddr   \n",$time,);
    intf.arid     = 0;
    intf.arvalid  = 0;
    intf.araddr   = 0;
    intf.arlen    = 0; 
    intf.arsize   = 0; 
    intf.arburst  = 0;

 $display("@%0t [axi_driver]  read_addr_to_slave ended\n",$time);
endtask
  
task datareceive(input [17:0] RID, output  data);
 $display("@%0t [axi_driver]  read_data_from_slave started \n",$time);
   // vif.cb.axi2ddr_rid     <=  RID;
     @(posedge intf.clk);
     
    wait (intf.rvalid === 1'b1);
$display("intf.rvalid at driver is %d",intf.rvalid);
      data= intf.rdata; 
   intf.rready  = 1;
   
   $display("rdata at driver is %d",data);
    @(posedge intf.clk);
    intf.rready  = 0;
 //$display("@%0t [axi_driver]  read_data_from_axi ended\n",$time);
endtask


endclass : axi_driver 