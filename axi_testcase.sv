class axi_testcase extends ovm_test;
axi_sequence seq;
axi_sequencer sequencer;
axi_environment env;
//registering
`ovm_component_utils(axi_testcase)

//constructor
function new(string name = "env", ovm_component parent= null);
super.new(name,parent);
endfunction : new

function void build();
  super.build();
  env = axi_environment::type_id::create("env",this);
endfunction : build

task run();

sequencer=env.agent.seqencer;
sequencer.count = 0;
  seq = axi_sequence::type_id::create("seq");
seq.start(sequencer,null);

endtask 

endclass : axi_testcase