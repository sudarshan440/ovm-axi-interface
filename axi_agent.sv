class axi_agent extends ovm_agent;
axi_monitor  monitor;
axi_driver driver;
axi_sequencer seqencer;
ovm_analysis_port #(axi_sequence_item) agent_tb;
ovm_analysis_port #(axi_sequence_item) agent_dut;

//registering
`ovm_component_utils(axi_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);

endfunction : new

function void build();
super.build();
agent_tb = new("agent_tb",this);
agent_dut = new("agent_dut",this);
seqencer = axi_sequencer::type_id::create("sequencer",this);
driver = axi_driver::type_id::create("driver",this);
monitor = axi_monitor::type_id::create("monitor",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(seqencer.seq_item_export);
monitor.mon2sbwrite.connect(agent_tb);
monitor.mon2sbread.connect(agent_dut);
endfunction : connect


endclass : axi_agent