module axi_slave( aclk, aresetn, awaddr, awvalid, awready,awlen,wlast,awsize,awburst,
wid,wstrb,bid,arlen,arsize,arburst,rid,rlast, wdata, awid,wvalid, wready, bresp, 
bvalid, bready, araddr, arid,arvalid, arready, rresp,rdata, rvalid, rready );
  
  
 parameter ADDR_WIDTH =32;
 parameter DATA_WIDTH= 32; 
 parameter MEM_SIZE   =16;
 parameter AXI_MAS_ID =18;
 parameter AXI_WID =4;
 parameter MEM_WIDTH = 32;
  parameter MEM_DEPTH = 1024;
  parameter BRESP_WIDTH = 2;
  
  parameter WIDLE=4'b0001;
  parameter WADDR=4'b0010;
  parameter WDATA=4'b0100;
  parameter WRESP=4'b1000;
   
  parameter RIDLE=4'b0001;
  parameter RADDR=4'b0010;
  parameter RDATA=4'b0100;
  
  
 
  input wire aclk;
  input wire aresetn;
  input wire wlast;
  input wire [ AXI_WID -1:0]  awid;
  input wire [ AXI_WID -1:0]  arid;
  input wire [3:0] awlen;
  input wire [2:0] awsize;
  input wire [1:0] rresp;
   
  output reg awready;
  output reg wready;
  output reg [1:0] bresp;
  output reg bvalid;
  input wire [ AXI_WID -1:0]  wid;
  input wire [3:0] wstrb;
  input wire [ AXI_WID -1:0]  bid;
  input wire [1:0] awburst; 
   output reg arready;
  input [1:0] arburst;
  output reg rvalid;
  output reg [ DATA_WIDTH-1:0] rdata; 
  input wire [ ADDR_WIDTH-1:0] awaddr;
  input wire awvalid;
  input [ AXI_WID -1:0]  rid;
  input wire wvalid;
  input wire rlast;
  input wire [ DATA_WIDTH-1:0] wdata;
  input wire bready;
  input wire [ ADDR_WIDTH-1:0] araddr;
  input wire [2:0] arsize;
  input wire [3:0] arlen;
  input wire arvalid;
  input wire rready;
  wire rstart_trans;
  reg raddr_done;
  reg rdata_done;
   reg [3:0] wstate;
  reg [3:0] wnext_state;
  reg [3:0] rstate;
  reg [3:0] rnext_state;
  assign rstart_trans = arvalid;
  
  
  wire wstart_trans;
  reg waddr_done;
  reg wdata_done;
  reg wresp_done;
  reg [BRESP_WIDTH-1:0] temp_resp;
  
  reg [MEM_WIDTH-1:0] mem_wdata;
  reg [ADDR_WIDTH-1:0] mem_waddr;
  reg [ADDR_WIDTH-1:0] mem_raddr;
  reg [DATA_WIDTH-1:0] mem_rdata;
  reg [MEM_WIDTH-1:0] memory [0:MEM_DEPTH];
  
  assign wstart_trans = awvalid;
  
  integer i;
  initial begin
    waddr_done <= 0;
    wdata_done <= 0;
    wresp_done <= 0;
    awready <= 0;
    wready <= 0;
    bvalid <= 0;
  end
  
  always@(*) begin
    memory[mem_waddr] <= mem_wdata;
	$display("memory[mem_waddr] write is %d",memory[mem_waddr]);
  end
  
  
  always@( posedge aclk, negedge aresetn ) begin
    if( !aresetn ) begin
      wstate <= WIDLE;
    end else begin
      wstate <= wnext_state;
    end
  end
  
  always@(wstate or wstart_trans or waddr_done or wdata_done or wresp_done or awvalid or wvalid or bready ) begin
    case(wstate) 
      WIDLE: begin
        wresp_done <= 0;
        bvalid <= 0;
        wnext_state <= (wstart_trans)?WADDR : WIDLE;
      end
      WADDR: begin
        awready <= 1;
        mem_waddr <= awaddr;
		$display("waddr recieved at dut is %d",mem_waddr);
		$display("$time");
        waddr_done <= 1;
        wnext_state <= (waddr_done )? WDATA : WADDR;
		$display("next_state is %d",wnext_state);
      end
      WDATA: begin
        awready <= 0;
        wready <= 1;
        if(  wvalid ) begin
          mem_wdata <= wdata;
		  $display("wdata recieved at dut is %d",wdata);
          wdata_done <= 1;
        end else begin
          wdata_done <= 0;
        end
        wnext_state <= (wdata_done )? WRESP : WDATA;
      end
      WRESP: begin
        wready <=0;
        bvalid <= 1;
        bresp <= 0;
        if( bready  ) begin
          wresp_done <= 1;
        end else begin
          wresp_done <= 0;
        end
        wnext_state <= (wresp_done )? WIDLE : WRESP;
      end
      default : begin
        wnext_state <= WIDLE;
      end
    endcase 
  end
 
 
 /////////////////////////read 
 always@(*) begin
    mem_rdata <= memory[mem_raddr];
	$display("memory[mem_raddr] read is %d",memory[mem_raddr]);
  end
  
 always@( posedge aclk, negedge aresetn ) begin
    if( !aresetn ) begin
      rstate <= RIDLE;
    end else begin
      rstate <= rnext_state;
    end
  end
  
  always@(rstate or rstart_trans or raddr_done or rdata_done or arvalid or rready ) begin
    case(rstate) 
      RIDLE: begin
        rvalid <= 0;
        rnext_state <= (rstart_trans)?RADDR : RIDLE;
      end
      RADDR: begin
        arready <= 1;
        mem_raddr <= araddr;
		$display("araddr recieved at dut is %d",araddr);
        raddr_done <= 1;
        rnext_state <= (raddr_done )? RDATA : RADDR;
      end
      RDATA: begin
        arready <= 0;
        rvalid <= 1;
        rdata <= mem_rdata;
		$display("rdata recieved at dut is %d",rdata);
        if(  rready ) begin
          rdata_done <= 1;
        end else begin
          rdata_done <= 0;
        end
        rnext_state <= (rdata_done )? RIDLE : RDATA;
      end
      default : begin
        rnext_state <= RIDLE;
      end
    endcase 
  end

 
 
  
  
  /*
  axi_slave_write slave_write(
                    .aclk(aclk),
                    .aresetn(aresetn),
                    
                    .awaddr(awaddr),
                    .awvalid(awvalid),
                    .awready(awready),
                    
                    .wdata(wdata),
                    .wvalid(wvalid),
                    .wready(wready),
                    
                    .bresp(bresp),
                    .bready(bready),
                    .bvalid(bvalid)
  );
  
  axi_slave_read slave_read(
                    .aclk(aclk),
                    .aresetn(aresetn),
                    
                    .araddr(araddr),
                    .arvalid(arvalid),
                    .arready(arready),
                    
                    .rdata(rdata),
                    .rvalid(rvalid),
                    .rready(rready)
  );
  */
endmodule 