package axiPkg;
   //`include "ovm_pkg.sv"
  // import ovm_pkg::*;
   //`include "ovm_pkg.sv"
   //`include "ovm_macros.svh"
   typedef enum {WRITE,READ} wr_e;
 
 parameter ADDR_WIDTH =32;
  parameter DATA_WIDTH= 32; 
 parameter MEM_SIZE   =16;

 parameter AXI_MAS_ID =18;
 parameter AXI_WID =4;

  `include "ovm.svh"
  `include "axi_sequence_item.sv"
  `include "axi_sequence.sv"
  `include "axi_sequencer.sv"
  `include "Wrapper.sv"
  `include "axi_driver.sv"
  `include "axi_monitor.sv"
  `include "axi_agent.sv"
  `include "scoreboard.sv"
  `include "axi_envirnoment.sv"
endpackage : axiPkg
