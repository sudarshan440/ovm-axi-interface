class axi_monitor extends ovm_monitor;
axi_sequence_item seq_item;
virtual axi_intf intf;
Wrapper wrapper;
ovm_object dummy;
byte data_bytes[$];
bit ackreceived;
ovm_analysis_port #(axi_sequence_item) mon2sbwrite;
ovm_analysis_port #(axi_sequence_item) mon2sbread;


//registering
`ovm_component_utils(axi_monitor)
//constructor

bit [31:0]  data,addr;

covergroup cg;

data : coverpoint data;
addr : coverpoint addr;
  
endgroup : cg 

function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
cg = new;
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 

if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
mon2sbwrite = new("mon2sbwrite",this);
mon2sbread = new("mon2sbread",this);

end
else
ovm_report_info(get_type_name(),"dummy is really dummy");

endfunction : build

  bit [ ADDR_WIDTH -1:0] awaddr,araddr;
  bit [ DATA_WIDTH -1:0] wdata,rdata;
virtual task run();
forever
begin
writedatatransfer();
readdatareceived();
end
endtask

task writedatatransfer ();

@ (posedge intf.clk);
       // $display("@%0t [axi_monitor] waiting for axislave_awvalid == 1'b1 \n",$time);
		
        wait(intf.awvalid==1'b1);
        awaddr  = intf.awaddr;
     //   $display("@%0t [axi_monitor] waiting for axi2slave_wvalid == 1'b1 \n",$time);
        wait(intf.wvalid==1'b1);
        wdata = intf.wdata;
		data = intf.wdata;
		addr = intf.awaddr;
	    seq_item = axi_sequence_item::type_id::create("seq_item");
        seq_item.addr = awaddr;
        seq_item.data = wdata;
        cg.sample();		
	    mon2sbwrite.write(seq_item);  
	 
 
endtask
	
	 

task readdatareceived ();
        @(posedge intf.clk);
       // $display("@%0t [axi_monitor] waiting for axi2slave_arvalid == 1'b1 \n",$time);
        wait(intf.arvalid==1'b1);
	    araddr  = intf.araddr;
        //$display("@%0t [axi_monitor] waiting for slave2axi_rvalid == 1'b1 \n",$time);
        wait(intf.rvalid==1'b1);
	    rdata=intf.rdata;
        seq_item = axi_sequence_item::type_id::create("seq_item");
        seq_item.addr = araddr;
		seq_item.data = rdata;	
	    mon2sbread.write(seq_item);  
	 
 
endtask
		 
endclass : axi_monitor