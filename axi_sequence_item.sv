
 class axi_sequence_item extends ovm_sequence_item;
// variable declaration 


static int sid=0;
rand bit id;
rand bit  [ ADDR_WIDTH -1:0] addr;
rand bit  [ DATA_WIDTH -1:0] data;
wr_e wr;


 // registering with factory
`ovm_object_utils_begin(axi_sequence_item)
 `ovm_field_int(addr,OVM_ALL_ON);
`ovm_field_int(data,OVM_ALL_ON);
 `ovm_field_enum(wr_e,wr,OVM_ALL_ON);
`ovm_object_utils_end

//constructor
function new(string name = " " );
super.new(name);
endfunction: new

 
endclass : axi_sequence_item